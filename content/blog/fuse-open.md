---
title: Fuse Open
date: 2018-05-14 22:52:00 +0200
---

OK, so [the announcement](https://blog.fusetools.com/announcing-fuse-open-free-ea289bbf32d0)
that I hinted at in [my previous post]({{< relref "hello.md" >}})
is now public, so I can finally talk about this.

<!--more-->

## What

*I'm very happy to tell you that [Fuse](https://fuse-open.github.io) is now
fully open source software!* :tada:

For those who don't know what I'm talking about: Fuse is a mobile app
development environment, developed by Fusetools. You can read more about it
[here](https://fuse-open.github.io/features/).

This work represents more than 3.5 years of my life (and of course lots of
other people, but you know, this is *my* blog :smirk:), and I've been pushing
hard inside the company my whole time there to open source it. We already
open sourced [fuselibs](https://github.com/fuse-open/fuselibs) about a year
ago, and as far as I'm concerned, that was a success.

Now, there's some sad news that comes with this as well. Fusetools will no
longer be working on Fuse. That's now up to the community. The company is
switching focus to an [app-as-a-service](https://www.fusetools.com/apps)
business model.

This also marks the end of my time at Fuse. I'll wrap up my end of Fuse 1.9
and then take a few weeks of vacation, before I start my next job. What I'll
be doing next is very exciting, but deserve its own post.

## Why

There's many reasons why we ended up where we are now. I'll try to explain
what I believe are the major reasons below.

*Please note that the opinions stated here are my personal ones, and not
those of my employer. I'm writing this article to shed some light on what
I believe is the reasons we've come to where we are, not to blame anyone,
but to try to learn from our mistakes.*

First and foremost, we failed to build a viable business on selling
development tools. Developing a full platform, comprising of compilers,
standard libraries, editor-plugins, debugging tools, IDEs etc is a big
undertaking, and cost a lot of money, while the market is full of free
tools that does a pretty decent job. To convince someone to pay for something
they can get for free somewhere else, you need to be a *lot* better.

Secondly, we've been taking a lot of time-consuming detours. We've started
projects that, in my opinion, should never have been started. We should have
kept our main focus enabling our users to do more, not to do more for them.

Third, I sadly think we failed to be *as good as we needed to*. This one is a
bit touchy, but I'll try to explain. I'm not saying I think we did bad work,
far from it. But to make up for the first point, we would have needed to be
vastly better than the competition. But due to the second point, the
competition largely caught up to us.

## What could we have done differently?

I think the problem we set out to solve was a hard one, so the short answer
is "beats me"... :confused:

However, I believe that our chances would at least have been *better* if we
open sourced the platform *from the beginning*.

The reason is that *we might not have had to* build the platform all by
ourselves. If we would for instance made our package-manager usable for
shipping 3rd-party packages, we could have leveraged the community more at
more of the high-level work. Our company could have focused on functionality
that enabled the community. Instead we ended up doing a lot of very
high-level work that didn't end up benefiting a lot of users.

I also believe that if we made the Uno compiler a stand-alone C# to C++
transpiler, and did all the graphics functionality and app-bootstrapping
as library functionality instead of compiler-internals, the compiler might
have been useful for other C#-oriented projects, and we could have aligned
closer with the C# community than we ended up. Instead, we treated Uno as
an internal tool that app-developers weren't really supposed to care about.

I'm far from sure that those changes together would have been enough,
though. This was a hard problem to solve, and that was exactly why I was
interested in this gig to begin with.

## The future

Well, I hope that the future of Fuse is bright, but different than before.
Thanks to Fusetools releasing all of the code, I do believe that the project
will live on, with a better future that isn't meddled with short-term wins
that hurt in the long run.

I plan on continuing at least some of my work on Uno / Fuselibs, and I'm not
the only one. In addition, there's also still some commercial interest for
Fuse, just not from Fusetools. Anything in this area is still not announced,
but expect to hear more.

I still believe the technology is good, and hopefully now that it's finally
all out there for anyone to play with, we can get some great work done! Fuse
has a unique tech-stack, with a declarative UI engine that is built around
preview / live-refresh, and a comprehensive toolkit. The turn-around time for
experimenting is still miles beyond the competition. :sparkles:
