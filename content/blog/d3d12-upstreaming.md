---
title: "OpenGL on DirectX: Conformance & upstreaming of the D3D12 driver"
slug: d3d12-upstreaming
date: 2021-03-10 20:30:28 +0100
tags: [D3D12, OpenGL, OpenCL, Mesa]
canonical_url: "https://www.collabora.com/news-and-blog/news-and-events/opengl-directx-conformance-upstreaming-d3d12.html"
---
Over the last year and then so, we at Collabora have been working with
Microsoft on their [D3D12 mapping layer], which I announced in my
[previous blog post]. In July, Louis-Francis Ratté-Boulianne [wrote an
update][d3d12-update] on the status on the Collabora blog, but a lot has
happened since then, so it's time for another update.

<!--more-->

There's two major things that has happened since then; we have passed the
OpenGL 3.3 conformance tests, and we have upstreamed the code in [Mesa 3D].

## Photoshop support

It might not be a big surprise, but one of the motivation for this work was
to be able to run applications like Photoshop on Windows devices without full
OpenGL support.

I'm happy to report that Microsoft has released their [compatibility pack]
that uses our work to provide OpenGL (and OpenCL) support, [Photoshop can now
run on Windows on ARM CPUs][photoshop on ARM]! This is pretty exciting to see
high-profile applications like that benefit from our work!

## OpenGL 3.3 Conformance Test Suite

First of all, I would like to point out that having passed the OpenGL
{{< abbr title="Conformance Test Suite" >}}CTS{{< /abbr >}} isn't necessarily the same as
being formally conformant. There's some details about how to formally
becoming conformant on layered implementations that are complicated, and I'll
leave the question about formal conformance up to Microsoft and Khronos.

Instead, I want to talk a bit about *passing the OpenGL CTS*.

### Challenges for layered implementations

A problem we face with layered implementations is that we are subject to a
few more sources of issues, some of which are entirely out of our control.
A normal OpenGL, non-layered, implementation have two primary sources of
issues:

1. The OpenGL driver
2. The hardware

Issues with the OpenGL driver itself that leads to tests failing is always
required to be fixed before results are submitted. Issues with the hardware
*generally* requires software workarounds, but this is not always feasible,
so Khronos have a system where a vendor can file a [waiver] for a hardware
issue, and if approved they can mark test-failures as waived, and the
appropriate failures will be ignored.

So far so good.

But for our layered implementations, our world looks a bit different. We
don't really see the hardware, but instead we see D3D12 and the D3D12
driver. This means our sources of issues are:

1. The OpenGL driver
2. The D3D12 run-time
3. The D3D12 vendor-driver
4. The hardware

For the OpenGL driver, the story is the same as for a non-layered
implementation, but from there on things start changing.

Problems in the D3D12 run-time must also be fixed before submitting results.
We work together with Microsoft to get these issues fixed as appropriate.
Such fixes can take a while to trickle all the way into a Windows build and
to end-users, but they will eventually show up.

But for the D3D12 vendor-driver and below, things gets complicated. First of
all, it's not always possible for us to tell vendor-driver issues and
hardware issues apart. And worse, as these are developed by third party
companies, we have little insight there. We can't affect their priorities,
so it's hard to know when or even if an issue gets resolved.

It's also not really a good idea to work around such issues, because if they
turn out to be fixable software problems, we don't know when they will be
fixed, so we can't really tell when to disable the work-around. We also don't
know exactly what combination of hardware and software these issues apply to.

But there's one case where we have full insight, and that's when the D3D12
vendor-driver is [WARP], a high-performance software rasterizer. Because that
component is developed by Microsoft, and we have channels to report issues
and even make sure they get resolved!

### Bugs, bugs, bugs

When developing something new, there's always going to be bugs. But usually
also when using something existing in a new way. We encountered a lot of bugs
on our way, and here's a quick overview over some of them. This is in no way
exhaustive, and most of our own bugs are not that interesting. So this is
mostly about problems unique to layered implementations.

#### 64-bit shifts

It turned out early on that the DXIL validator in D3D12 had a requirement
when parsing the LLVM bitcode that required that the amounts were always
32-bit values. While this seems fine by itself, LLVM itself requires that
all operands to binops have the same bit-size. This obviously meant that
only 32-bit shifts could pass the validator.

Microsoft quickly removed this requirement once we figured out what was
going on.

#### Aligned block-compressed textures

In D3D12, one requirement for block-compressed textures is that the
base-level is aligned to ther block-size. This requirement does not apply
to mip-levels, and OpenGL has no such requirement. This isn't technically
speaking a bug, but a documented limitation in DirectX.

It turns out that this limitation was an artificial historical left-over,
and after a bunch of testing (and fixing of WARP), we got this limitation
lifted. Great :)

### D3D12 vendor-driver bugs

Something that has been much more frustrating is bugs in the
vendor-drivers. The problem here is that even though we have channels to
file bugs, we don't have any influence or even insight into their
prioritization and release schedule.

I think it suffice to say that there's been several reported bugs to all
vendors we've actively been running the OpenGL CTS on top of. We believe
fixes are underway for at least some of this, but we can't make any promises
here.

### Current status

Right now, the only configurations we're cleanly passing the OpenGL 3.3
CTS on are WARP (which [became conformant on November 24th, 2020][submission 279]),
and NVIDIA (which [became conformant on February 26th, 2021][submission 288]).

Having these multiple independent implementations of DirectX drivers passing
in conjunction with the Mesa/D3D12 layer shows that we are able to implement
GLon12 in a vendor-neutral way, which allowed us to bring the layer to
conformance. Many thanks to Khronos for their assistance through this process.

We've also submitted results on top of an Intel GPU, but that submission
has been halted due to failures, and will as far as I know be updated as
soon as Intel publish updated drivers.

The conformance tests have been run against our [downstream fork], which is
no longer actively maintained, because:

## Upstreaming

The D3D12 driver was upstreamed in Mesa in [Merge-Request 7477], and the
OpenCL compiler followed in [Merge-Request 7565]. There's been [a lot more
merge-requests][D3D12 merge requests] since then, and even more is expected
in the future.

The process of upstreaming the driver into Mesa3D went relatively smoothly,
but there were quite a lot of regressions that happened quickly after we
upstreamed the code, so to avoid this from becoming a big problem we've
added the D3D12 driver to Mesa's set of GitLab CI tests. We now build and
test the D3D12 driver on top of WARP on CI, as well as running some basic
sanity-tests for the OpenCL compiler.

All in all, this seems to work very well right now, and we're looking
forward to the future. Next step, [WSL support]!

I'm no longer working full-time on this project, instead I'm trying to
take some of the lessons learned and apply them to [Zink]. I'm sure there's
even more room for code-reuse than what we currently have, but it will
probably take some time to figure out.

[D3D12 mapping layer]: https://devblogs.microsoft.com/directx/in-the-works-opencl-and-opengl-mapping-layers-to-directx
[previous blog post]: {{< relref "d3d12-mesa-driver/index.md" >}}
[d3d12-update]: https://www.collabora.com/news-and-blog/blog/2020/07/09/deep-dive-into-opengl-over-directx-layering/
[compatibility pack]: https://devblogs.microsoft.com/directx/announcing-the-opencl-and-opengl-compatibility-pack-for-windows-10-on-arm/
[photoshop on ARM]: https://feedback.photoshop.com/conversations/photoshop-beta/photoshop-beta-for-windows-arm-is-here/5fb3584b38150f55db64b1e4
[waiver]: https://github.com/KhronosGroup/VK-GL-CTS/tree/master/external/openglcts#waivers
[Mesa 3D]: https://www.mesa3d.org/
[WARP]: https://docs.microsoft.com/en-us/windows/win32/direct3darticles/directx-warp
[submission 279]: https://www.khronos.org/conformance/adopters/conformant-products/opengl#submission_279
[submission 288]: https://www.khronos.org/conformance/adopters/conformant-products/opengl#submission_288
[downstream fork]: https://gitlab.freedesktop.org/kusma/mesa/-/commits/msclc-d3d12
[Merge-Request 7477]: https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/7477
[Merge-Request 7565]: https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/7565
[D3D12 merge requests]: https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests?scope=all&state=all&label_name[]=d3d12
[WSL support]: https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/7937
[Zink]: {{< relref "introducing-zink/index.md" >}}
