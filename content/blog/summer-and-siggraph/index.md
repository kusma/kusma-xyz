---
title: "Zink: Summer Update and SIGGRAPH 2019"
slug: summer-and-siggraph
date: 2019-07-25 21:38:36 +0200
tags: [Zink, OpenGL, Vulkan, Mesa, SIGGRAPH]
canonical_url: "https://www.collabora.com/news-and-blog/blog/2019/07/25/zink-summer-update-and-siggraph-2019/"
---
Here's an overview of the recent changes in Zink since [the previous post][1],
as well as an exciting announcement!

<!--more-->

## What's new in the world of Zink?

OK, so I haven't been as good at making frequent updates on as I was
hoping, but let's try to make up for it:

Since last time, there's quite a lot of things that has been resolved:
- We now do proper control-flow. This means things like if-statements,
  for-loops etc. There might be some control-flow primitives missing
  still, but that's because I haven't encountered any use yet.
- Alpha testing has been implemented.
- Client-defined clip-planes has been implemented.
- Support for `gl_FrontFacing` has been implemented.
- Lowering of `glPointSize()` to `gl_PointSize` has been implemented.
  This means you can use `glPointSize()` to specify sizes instead of
  having to write the `gl_PointSize`-output from the vertex shader.
- Support for `gl_FragDepth` has been implemented.
- Two-sided lighting has been implemented.
- Shadow-samplers has been implemented.
- Support for 8-bit primitive indices has been implemented.
- Occlusion queries has been implemented correctly across command
  buffers. This includes the ability to pause / restore queries.
- The compiler has been ported to C.
- The compiler no longer lowers IO, but instead process derefs
  directly.
- The compiler now handles booleans properly. It's no longer lowering
  them to floats.
- David Airlie has contributed lowering of `glShadeModel(GL_FLAT)` to
  `flat` [interpolation qualifiers][2]. This still doesn't give us the
  right result, because Vulkan [only supports the first vertex][3] as
  the provoking vertex, and OpenGL [defaults to the last one][4]. To
  resolve this in a reasonable way, we need to inject a geometry shader
  that reorders the vertices, but this hasn't been implemented yet. You
  can read more about this in [this ticket][5]
- ... and a boat-load of smaller fixes. There's just too many to
  mention, really.

In addition to this, there's been a pretty significant rewrite, changing
the overall design of Zink. The reason for this, was that I made some
early design-mistakes, and after having piled a bit too many features on
top of this, I decided that it would be better to get the fundamentals
right first.

Sadly, not all features have been brought forward since the rewrite, so
we're currently back to OpenGL 2.1 support. Fixing this is on my list of
things I want to do, but I suspect that cleaning things up and upstreaming
will take presedence over OpenGL 3.0 support.

And just to give you something neat to look at, here's [Blender][6] running
using Zink:

{{< figure src="images/blender.png" title="Blender on Zink" >}}

## Khronos Vulkan BoF at SIGGRAPH 2019

[Khronos][7] has been nice enough to give me a slot in their Vulkan Sessions
at the [Khronos BoFs][8] during [SIGGRAPH 2019][9]!

The talk will be a slightly less tech-heavy introduction to Zink, what it
does and what the future holds. It will focus more on the motivation and
use cases than the underlying technical difficulties compared to my previous
talks.

So, if you're in the area please drop by! A conference pass is not required
to attend the BoF, as it's not part of the official SIGGRAPH program.

[1]: {{< relref "../introducing-zink/index.md" >}}
[2]: https://www.khronos.org/opengl/wiki/Type_Qualifier_(GLSL)#Interpolation_qualifiers
[3]: https://vulkan.lunarg.com/doc/view/1.0.26.0/linux/vkspec.chunked/ch23s01.html
[4]: https://www.khronos.org/registry/OpenGL/specs/gl/glspec45.core.pdf#section.13.4
[5]: https://gitlab.freedesktop.org/kusma/mesa/issues/15
[6]: https://www.blender.org/
[7]: https://www.khronos.org/
[8]: https://www.khronos.org/events/2019-siggraph
[9]: https://s2019.siggraph.org/
