---
title: "Hello World!"
slug: hello
date: 2018-05-13 11:36:58 +0200
---
Welcome to my new blog, where I'll be writing about my open source development.

<!--more-->

This is just a short announcement post, to introduce this blog.

If you don't already know who I am, I am Erik "kusma" Faye-Lund, a Norwegian
graphics programmer.

I'm currently working on the following projects, which I expect to be posting
about:

- [Fuselibs](https://github.com/fusetools/fuselibs-public): This is the core
  UI engine in [Fuse](https://www.fusetools.com), where I currently work.
- [Rocket](https://rocket.github.io): This is a tool for tweaking
  synchronization of audio and visuals in demoscene productions.
- [Grate](https://github.com/grate-driver): This is a reverse-engineered GPU
  driver for the GeForce ULP in the NVIDIA Tegra 2/3/4 SoCs (code-name:
  AR20 / Aurora).
- [Mesa 3D](https://www.mesa3d.org): This is what Grate is being built on top,
  and the goal of Grate is ultimately to upstream a driver into Mesa.

In addition to this, there's some cool new announcements coming up very soon!
So stay tuned!
