---
title: "Zink: Summer 2021 Update"
date: 2021-06-14 20:00:00 +0100
tags: [Zink, OpenGL, Vulkan, Mesa]
canonical_url: "https://www.collabora.com/news-and-blog/blog/2021/06/14/zink-summer-2021-update/"
---
There's a lot that has happened in the world of Zink since my [last
update], so let's see if I can bring you up to date on the most
important stuff.


<!--more-->

## Upstream development

Gosh, when I last blogged about Zink, it hadn't even landed upstream
in Mesa yet! Well, by now it's been upstream for quite a while, and most
development has moved there.

At the time of writing, we have [merged 606 merge-requests][merged-mrs]
labeled "zink". The current tip of mesa's main branch is totaling 1717
commits touching the `src/gallium/drivers/zink/` sub-folder, written by
42 different contributors. That's pretty awesome in my eyes, Zink has
truly become a community project!

Another noteworthy change is that [Mike Blumenkrantz][zmike] has come
aboard the project, and has churned out an incredible amount of
improvements to Zink! He got hired by [Valve] to work on Zink (among
other things), and is now the most prolific contributor, with more than
twice the amount of commits than I have written.

If you want a job in Open Source graphics, Zink has a proven
track-record as a job-creator! :smile:

In addition to Mike, there's some other awesome people who have been
helping out lately.

{{< figure src="images/hl2.png" title="Half-Life 2 running with Zink." >}}

## OpenGL 4.6 support

Thanks to a lot of hard work by Mike assisted by [Dave Airlie][airlied]
and [Adam Jackson][ajax], both of [RedHat], Zink is now able to expose
the OpenGL 4.6 (Core Profile) feature set, given
[enough Vulkan features][zink-features]! :tada:

Please note that this doesn't mean that Zink is yet a *conformant*
implementation, there's some details left to be ironed out before we can
claim that. In particular, we need to pass the conformance tests, and
submit a conformance report to Khronos. We're not there yet.

I'm also happy to see that Zink is currently at the top of [MesaMatrix]
(together with LLVMpipe, i965 and RadeonSI), reporting a total of 160
OpenGL extensions at the time of writing!

In theory, that means you can run any OpenGL application you can think of
on top of Zink. Mike is hard at work testing the entire Steam game
library, and things are working pretty well.

Is this the end of the line for Zink? Are we done now? Not at all!
:laughing:

### OpenGL compabibility profile

We're still stuck at OpenGL 3.0 for compatibility contexts, mainly due to
lack of testing. There's a lot of features that need to work together in
relatively complicated ways for this to work for us.

Note that this only matters for applications that rely on legacy OpenGL
features. Modern OpenGL programs gets OpenGL 4.6 support, as mentioned
previously.

I don't *think* this is going to be a big deal to enable, but I haven't
spent time on it.

## OpenGL ES 3.1 support

Similar to the OpenGL 4.6 support, we're now able to expose the OpenGL ES
3.1 feature set. This is again thanks to a lot of hard work by Mike and
the gang.

Why not OpenGL ES 3.2? This comes down to the
[`GL_KHR_blend_equation_advanced`][gl-advanced-blend] feature. Mike
[blogged about the issue][not-es] a while ago.

## Lavapipe and continuous integration

To prevent regressions, we've started testing Zink on the [Mesa CI]
system for every change. This is made possible thanks to Lavapipe, a Vulkan
software implementation in Mesa that reuse the rasterizer from LLVMpipe.

This means we can run tests on virtual cloud machines without having to
depend on unreliable hardware. :robot:

At the time of writing, we're only exposing OpenGL 4.1 on top of Lavapipe,
due to some lacking features. But we have patches in the works to bring
this up to OpenGL 4.5, and OpenGL 4.6 probably won't be far off when that
lands.

## Windows support

Basic support for [Zink on Microsoft Windows][windows-port] has landed.
This isn't particularly useful at the moment, because we need better
window-system integration to get anywhere near reasonable performance.
But it's there.

## macOS support

Thanks to work by [Duncan Hopkins][duncan] of [The Foundry], there's also
some support for macOS. This uses [MoltenVK] as the Vulkan implementation,
meaning that we also support the [Vulkan Portability Extension] to some
degree.

This support isn't quite as drop-in as on other platforms, because it's
completely lacking window-system integration. But it seems to work for
the use-cases they have at The Foundry, so it's worth mentioning as well.

## Driver support

Beyond this, [Igalia] has brought up [Zink on the V3DV][v3dv] driver, and
I've heard some whispers that there's some people running Zink on top of
[Turnip], an open-source Vulkan driver for recent Qualcomm Adreno GPUs.

I've heard some people have some success getting things running on NVIDIA,
but there's a few obvious problems in the way there due to the lack of
proper DRI support... Which brings us to:

## Window System Integration

Another awesome new development is that Adam is working on [Penny]. So,
what's Penny?

Penny is another way of bringing up Zink, on systems without DRI support.
It works as a dedicated GLX integration that uses the
[`VK_KHR_swapchain`][vk-swapchain] extension to integrate properly with
the native Vulkan driver's window-system integration instead of Mesa baking
its own.

This solves a lot of small, nasty issues in the DRI code-path. I'll say
the magic "implicit synchronization" word, and hope that scares away
anyone wondering what it's about.

## Performance

A lot more has happened on the performance front as well, again all thanks
to Mike. However, much of this is still out-of-tree, and waiting in Mike's
[zink-wip] branch.

So instead, I suggest you check out [Mike's blog][zmike] for the latest
performance information (and much more up-to-date info on Zink). There's
been a lot going on, and I'm sure there's even more to come!

## Closing words

I think this should cover the most interesting bits of development.

On a personal note, I recently became a dad for the first time, and as a
result I'll be away for a while on paternity leave, starting early this
fall. Luckily, Zink is in good hands with Mike and the rest of the
upstream community taking care of things.

I would like to again plug [Mike's blog][zmike] as a great source of
Zink-related news, if you're not already following it. He posts a lot
more frequent than I do, and he's also an epic meme master, so it's all
great fun!

[last update]: {{< relref "../zink-fall-update.md" >}}
[merged-mrs]: https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=zink
[zmike]: https://www.supergoodcode.com/
[Valve]: https://www.valvesoftware.com/en/about
[airlied]: https://airlied.blogspot.com/
[ajax]: https://ajaxnwnk.blogspot.com/
[RedHat]: https://www.redhat.com/
[zink-features]: https://docs.mesa3d.org/drivers/zink.html#features
[MesaMatrix]: https://mesamatrix.net/
[gl-advanced-blend]: https://www.khronos.org/registry/OpenGL/extensions/KHR/KHR_blend_equation_advanced.txt
[not-es]: https://www.supergoodcode.com/notES/
[duncan]: https://twitter.com/aasimon3d
[The Foundry]: https://www.foundry.com/
[MoltenVK]: https://github.com/KhronosGroup/MoltenVK
[Vulkan Portability Extension]: https://www.khronos.org/blog/fighting-fragmentation-vulkan-portability-extension-released-implementations-shipping
[Mesa CI]: https://docs.mesa3d.org/ci/index.html
[windows-port]: https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/7432
[Igalia]: https://www.igalia.com/
[v3dv]: https://blogs.igalia.com/itoral/2020/11/05/v3dv-zink/
[Turnip]: https://gitlab.freedesktop.org/mesa/mesa/-/tree/main/src/freedreno/vulkan
[Penny]: https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/7661
[vk-swapchain]: https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_KHR_swapchain.html
[zink-wip]: https://gitlab.freedesktop.org/zmike/mesa/-/tree/zink-wip
