---
title: "Zink: Fall Update"
date: 2019-10-24 15:22:00 +0100
tags: [Zink, OpenGL, Vulkan, Mesa]
canonical_url: "https://www.collabora.com/news-and-blog/blog/2019/10/24/zink-fall-update/"
---
I recently went to [XDC 2019][xdc19], where I gave [yet another talk][xdc-talk]
about Zink. I kinda forgot to write a blog-post about it, so here's me trying
to make up for it... or something like that. I'll also go into some more
recent developments as well.

<!--more-->

My presentation was somewhat similar to the [talk I did][siggraph-talk] at
[SIGGRAPH][siggraph19] this year, but with a bit more emphasis on the technical
aspect, as the XDC audience is more familiar with [Mesa][mesa] internals.

If you're interested, you can find the slides for the talk [here][xdc-slides].
The talk goes through the motivation and basic approach. I don't think I need
to go through this again, as I've [already covered that][zink-why] before.

As for the status, Zink currently supports OpenGL 2.1 and OpenGL ES 2.0. And
there's no immediate plans on working on OpenGL 3.0 and OpenGL ES 3.0 until
Zink is upstream.

Which gets us to the more interesting bit; that I started working on
upstreaming Zink. So let's talk about that for a bit.

## Upstreaming Zink

So, my current goal is to get Zink upstream in [Mesa][mesa]. The plan outline
in my XDC talk is slightly outdated by now, so here I'll instead say what's
actually happened so far, and what I hope will follow.

Before I could add the driver itself, I decided to send all changes outside of
the driver as a set of merge-requests:

* [glBitmap R8 textures][zink-r8]
* [loader/dri3: do not blit outside old/new buffers][zink-blit-outside]
* [gallium/u_blitter: set a more sane viewport-state][zink-viewport]
* [nir: initialize uses_discard to false][zink-uses_discard]
* [lowering passes from Zink][zink-lowering-passes]

The last one is probably the most interesting one, as it moves a lot of
fixed-function operations into the state-tracker, so individual drivers won't
have to deal with them. Unless they choose to, of course.

But all of these has already been merged, and there's just one final
merge-request left:

* [Introduce Zink: OpenGL on Vulkan][zink-merge]

This merge-request adds the driver in its current state. It consists of 163
commits at the time of writing, so it's not a thing of beauty. But new drivers
usually aren't, so I'm not too worried.

When this is merged, Zink will finally be a "normal" part of Mesa... Well,
sort of anyway. I don't think we'll enable Zink to be built by default for a
while. But that'll just be a matter of adding `zink` to the
`-Dgallium-drivers` meson-option.

### Testing on CI

The current branch only adds building of Zink to the CI. There's no testing
being done yet. The reasons for this is two-fold:

1. We need to get a running environment on CI. Rather of bringing up some
   hardware-enabled test-runner, I intend to try to set up
   [SwiftShader][swiftshader] as a software rasterizer instead, as that
   supports Vulkan 1.1 these days.
2. We need some tests to run. Zink currently only supports OpenGL 2.1, and
   sadly [the conformance test suite][cts] doesn't have *any* tests for OpenGL
   versions older than 3.0. [Piglit][piglit] has some, but a full piglit-run
   takes significantly more time, which makes it tricky for CI. So right now,
   it seems the OpenGL ES 2.0 conformance tests are our best bet. We'll of
   course add more test-suites as we add more features.

So, there's some work to be done here, but it seems like we should be able to
get something working without *too much* hassle.

## Next steps

After Zink is upstream, it will be maintained similarly to other Mesa drivers.
Practically speaking, this means that patches are sent to the upstream repo
rather than my fork. It shouldn't make a huge difference for most users.

The good thing is that if I go away on vacation, or are for some other reason
unavailable, other people can still merge patches, so we'd slightly reduce the
*technical* bus-factor.

I'm not stopping developing Zink at all, but I have other things going on in
my life that means I might be busy with other things at times. As is the case
for everyone! :wink:

In fact, I'm very excited to start working on OpenGL 3.x and 4.x level
features; we still have a few patches for some 3.x features in some older
branches.

The future is bright! :sunny:

[cts]: https://github.com/KhronosGroup/VK-GL-CTS
[mesa]: https://www.mesa3d.org/
[piglit]: https://piglit.freedesktop.org/
[siggraph-talk]: {{< relref "summer-and-siggraph/index.md" >}}
[siggraph19]: https://s2019.siggraph.org/
[swiftshader]: https://swiftshader.googlesource.com/SwiftShader
[xdc-slides]: https://xdc2019.x.org/event/5/contributions/329/attachments/433/687/XDC2019-Zink-slide-deck.pdf
[xdc-talk]: https://xdc2019.x.org/event/5/contributions/329/
[xdc19]: https://xdc2019.x.org/
[zink-blit-outside]: https://gitlab.freedesktop.org/mesa/mesa/merge_requests/2189
[zink-lowering-passes]: https://gitlab.freedesktop.org/mesa/mesa/merge_requests/2213
[zink-merge]: https://gitlab.freedesktop.org/mesa/mesa/merge_requests/2363
[zink-r8]: https://gitlab.freedesktop.org/mesa/mesa/merge_requests/2188
[zink-uses_discard]: https://gitlab.freedesktop.org/mesa/mesa/merge_requests/2195
[zink-viewport]: https://gitlab.freedesktop.org/mesa/mesa/merge_requests/2190
[zink-why]: {{< relref "introducing-zink/index.md#why-implement-opengl-on-top-of-vulkan" >}}
