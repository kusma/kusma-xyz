---
title: "Working at Collabora"
date: 2018-07-31 12:01:00 +0200
---
In the [Fuse Open]({{< relref "fuse-open.md" >}}) post,
I mentioned that I would no longer be working at Fuse. I didn't mention what I
was going to do next, and now that it's been a while I guess it's time to let
the cat out of the bag: I've started working at
[Collabora](https://www.collabora.com/).

<!--more-->

I've been working here for 1.5 months now, and I'm really enjoying it so far!
I get to work on things I really enjoy, and I get a lot of freedom!
:smile: :tada:

## What is Collabora

Collabora is an Open Source consultancy, specializing in a few industries. Most
of what Collabora does is centered around things like Linux, automotive,
embedded systems, and multimedia. You can read more about Collabora 
[here](https://www.collabora.com/about-us/who-we-are/).

The word "consultant" brings out quite a lot of stereotypes in my mind.
Luckily, we're not that kind of consultants. I haven't worn a tie a single
day at work yet!

When I got approached by Collabora, I was immediately attracted by the
prospect of working more or less exclusively on Open Source Software.
Collabora has "Open First" as their motto, and this fits my ideology very
well! And trust me, Collabora really means it! :grinning:

## What will I be doing?

I'm hired as a Principal Engineer on the Graphics team. This obviously means
I'll be working on graphics technology.

So far, I've been working a bit on some R&D tasks about Vulkan, but mostly on
[Virgil 3D ("VirGL")](https://virgil3d.github.io/). If you don't know what
VirGL is, the very short explanation is that it's GPU virtualization for
virtual machines. I've been working on adding/fixing support for OpenGL 4.3 as
well as OpenGL ES 3.1. The work isn't complete but it's getting close, and
patches are being upstreamed as I write this.

I'm also working on [Mesa](https://www.mesa3d.org/). Currently mostly through
Virgil, probably through other projects in the future as well. Apart from
that, things depend heavily on customer needs.

## Working Remotely

A big change from my previous jobs, is that I now work from home Instead of a
shared office with my coworkers. This is because Collabora doesn't have an
Oslo office, as it's largely a distributed team.

I've been doing this for around 1.5 months already, and it works a lot better
than I feared. In fact, this was one of my biggest worries with taking this
job, but so far it hasn't been a problem at all! :tada:

But who knows, maybe all work and no play will make Jack a dull boy in the end?
:knife:

Jokes aside, if this turns out to be a problem in the long term, I'll look
into getting a desk at some co-working space. There's tons of them nearby.

## Working as a Contractor

Another effect of Collabora not having an Oslo office means that I have to
formally work as a contractor. This is mostly a formality (Collabora seems to
treat people the same regardless if they are normal employees or contractors),
but there's quite a lot of legal challenges on my end due to this.

I would definitely have preferred normal employment, but I guess I don't get
to choose all the details ;)

## Closing

So, this is what I'm doing now. I'm happy with my choice and I have a lot of
really great colleagues! I also get to work with a huge community, and as
part of that I'll be going to more conferences going forward (next up: 
[XDC](https://xdc2018.x.org/))!
